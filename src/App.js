import './App.css';

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  const listOfWeekdays = weekdays.map((data, key) =>{
    return (
    <tr>
      <td>{data}</td>
    </tr>
    )
  });

  return (
    <div className="App">
      <h2>W13 weekdays in table</h2>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody>
          {listOfWeekdays}
        </tbody>
      </table>
    </div>
  );
}

export default App;